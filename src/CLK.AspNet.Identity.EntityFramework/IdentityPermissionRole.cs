﻿using System;
using System.Collections.Generic;

namespace CLK.AspNet.Identity.EntityFramework
{
    /// <summary>
    /// 权限角色关系实体
    /// </summary>
    public class IdentityPermissionRole
    {
        // Properties
        public virtual string PermissionId { get; set; }

        public virtual string RoleId { get; set; }
    }
}