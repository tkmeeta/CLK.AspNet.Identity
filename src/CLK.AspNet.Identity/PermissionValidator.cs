﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace CLK.AspNet.Identity
{
    /// <summary>
    /// 权限验证
    /// </summary>
    /// <typeparam name="TPermission">权限</typeparam>
    /// <typeparam name="TKey">权限ID类型</typeparam>
    public class PermissionValidator<TPermission, TKey> : IIdentityValidator<TPermission>
        where TPermission : class, IPermission<TKey>
        where TKey : IEquatable<TKey>
    {
        // Constructors
        public PermissionValidator(IPermissionStore<TPermission, TKey> store)
        {
            #region Contracts

            if (store == null) throw new ArgumentNullException("store");

            #endregion Contracts

            // Default
            this.Store = store;
        }

        // Properties
        private IPermissionStore<TPermission, TKey> Store = null;

        /// <summary>
        /// 根据权限实体验证是否有权限
        /// </summary>
        /// <param name="permission">权限</param>
        /// <returns>返回IdentityResult操作结果</returns>
        public virtual async Task<IdentityResult> ValidateAsync(TPermission permission)
        {
            #region Contracts

            if (permission == null) throw new ArgumentNullException("permission");

            #endregion Contracts

            // Validate
            var errors = new List<string>();
            await this.ValidatePermissionName(permission, errors).WithCurrentCulture();
            if (errors.Count > 0) return IdentityResult.Failed(errors.ToArray());

            // Return
            return IdentityResult.Success;
        }

        private async Task ValidatePermissionName(TPermission permission, List<string> errors)
        {
            #region Contracts

            if (permission == null) throw new ArgumentNullException("permission");
            if (errors == null) throw new ArgumentNullException("errors");

            #endregion Contracts

            if (string.IsNullOrWhiteSpace(permission.Name))
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.PropertyTooShort, "Name"));
            }
            else
            {
                var owner = await this.Store.FindByNameAsync(permission.Name).WithCurrentCulture();
                if (owner != null && EqualityComparer<TKey>.Default.Equals(owner.Id, permission.Id) == false)
                {
                    errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.DuplicateName, permission.Name));
                }
            }
        }
    }
}