﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CLK.AspNet.Identity
{
    /// <summary>
    /// 权限的操作
    /// </summary>
    /// <typeparam name="TPermission"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public interface IPermissionStore<TPermission, in TKey> : IDisposable 
        where TPermission : class, IPermission<TKey>
    {
        // Methods
        Task CreateAsync(TPermission permission);
        
        Task UpdateAsync(TPermission permission);
        
        Task DeleteAsync(TPermission permission);
        
        Task<TPermission> FindByIdAsync(TKey permissionId);

        Task<TPermission> FindByNameAsync(string permissionName);
    }
}