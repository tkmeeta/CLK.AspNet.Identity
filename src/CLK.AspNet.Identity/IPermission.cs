﻿using System;
using System.Collections.Generic;

namespace CLK.AspNet.Identity
{
    /// <summary>
    /// 权限实体接口
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public interface IPermission<out TKey>
    {
        // Properties
        TKey Id { get; }
        /// <summary>
        /// 权限名称
        /// </summary>
        string Name { get; set; }
    }
}